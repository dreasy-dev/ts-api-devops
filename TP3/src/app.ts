import http from 'http';
import fs from 'fs';
import os from 'os';

const port = process.env.PING_LISTEN_PORT || 3000;

const logFilePath = 'ping_log.txt';

const server = http.createServer((req, res) => {
  if (req.url === '/ping' && req.method === 'GET') {
    const originalHost = req.headers.host;

    const logData = `Hostname: ${os.hostname()}, Original Host: ${originalHost}\n`;
    fs.appendFile(logFilePath, logData, (err) => {
      if (err) {
        console.error(`Error writing to log file: ${err}`);
      } else {
        console.log(`Logged: ${logData}`);
      }
    });

    res.writeHead(200, { 'Content-Type': 'application/json' });
    const responseJson = {
      headers: req.headers,
      originalHost: originalHost,
    };
    res.end(JSON.stringify(responseJson));
  } else {
    // Handle 404 errors
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end();
  }
});

console.log(os.hostname());

server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});
