# Project DevOps 

## I Requirement
- Docker 
- Git

## II Start project

            git clone  https://gitlab.com/dreasy-dev/ts-api-devops.git 
            docker compose up -d  


Si vous voulez changer le port d'écoute de l'api, il suffit de modifié le PING_LISTEN_PORT dans le docker-compose.yml.

Example: 
```bash 
docker compose up -d
```

Pour le Bonus, le fichier host de windows a été modifié pour ma part, mais de maniere plus simple il fallais juste changer le WP_HOME et SITEURL pour localhost:8080.
