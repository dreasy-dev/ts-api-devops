import http from 'http';

const port = process.env.PING_LISTEN_PORT || 3000;

const server = http.createServer((req, res) => {
  if (req.url === '/ping' && req.method === 'GET') {
    // Respond with JSON containing all headers
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end(JSON.stringify(req.headers));
  } else {
    // Handle 404 errors
    res.writeHead(404, { 'Content-Type': 'text/plain' });
    res.end();
  }
});

server.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});