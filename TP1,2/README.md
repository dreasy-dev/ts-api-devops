# Project DevOps 

## I Requirement
- Docker 
- Git

## II Start project

            git clone  https://gitlab.com/dreasy-dev/ts-api-devops.git    
            docker build -t ts-api-docker . 
            docker run -p 3000:3000 -e PING_LISTEN_PORT=3000 ts-api-docker 


Si vous Changer l'option PING_LISTEN_PORT il faut aussi changer le port d'ouverture du docker 3000:3000 par default

Example: 
```bash 
docker run -p 4000:4000 -e PING_LISTEN_PORT=4000 ts-api-docker
```


