
# Rendu TP-04



## Etape loadbalancer et 4 replicas:


            kubectl port-forward service/loadbalancer -n test-echo 80:80
            Forwarding from 127.0.0.1:80 -> 3000
            Forwarding from [::1]:80 -> 3000
            Handling connection for 80
            Handling connection for 80



## Etape 2: add ingress:

            kubectl get all -n test-echo
            NAME                                   READY   STATUS    RESTARTS   AGE
            pod/echo-deployment-585dc85bff-4tnjc   1/1     Running   0          126m
            pod/echo-deployment-585dc85bff-mqf79   1/1     Running   0          126m
            pod/echo-deployment-585dc85bff-n59w5   1/1     Running   0          126m
            pod/echo-deployment-585dc85bff-xv6qm   1/1     Running   0          126m
            pod/echo-replicaset-5hk9n              1/1     Running   0          152m
            pod/echo-replicaset-j5njx              1/1     Running   0          152m
            pod/echo-replicaset-tf7xn              1/1     Running   0          152m
            pod/echo-replicaset-wnslm              1/1     Running   0          152m

            NAME                   TYPE        CLUSTER-IP       EXTERNAL-IP   PORT(S)   AGE
            service/loadbalancer   ClusterIP   10.110.178.190   <none>        80/TCP    132m
            service/my-service     ClusterIP   10.97.62.205     <none>        80/TCP    133m

            NAME                              READY   UP-TO-DATE   AVAILABLE   AGE
            deployment.apps/echo-deployment   4/4     4            4           126m

            NAME                                         DESIRED   CURRENT   READY   AGE
            replicaset.apps/echo-deployment-585dc85bff   4         4         4       126m
            replicaset.apps/echo-replicaset              4         4         4       152m

avant de commencer il faut installer l'addon minikube tunnel et le start.

Suite a cela on peut deployer le ingress:

            kubectl apply -f pod.yaml

Et ensuite aller modifier le fichier /etc/hosts pour ajouter l'ip du loadbalancer:

Puis on peut voir la requete sur google en local en allant sur : 

            http://tp-4-wik.test.com/
            

![photo](./image.png)

        
